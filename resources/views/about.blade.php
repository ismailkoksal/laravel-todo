@extends("template")

@section("title", "A propos")

@section("content")
    <div class="container">
        <div class="card">
            <div class="card-body">
                <p>Dans ce TP j'ai appris à réaliser une application / site web dont le but était de faire de la prise
                    de note de « TODO » ou aussi appelé liste de tâches.</p>
                <p>Une TODO List est un procédé qui se veut simple et efficace pour gérer les tâches d'un projet. Ces
                    tâches peuvent être indépendantes ou devoir, au contraire, être accomplies dans un certain
                    ordre.</p>
                <p>Voilà la liste des fonctionnalités de l’application que j'ai créée :</p>
                <ul>
                    <li>Lister les tâches.</li>
                    <li>Ajouter une tâche.</li>
                    <li>Marquer comme terminé une tâche.</li>
                    <li>Suppression d'une tâche.</li>
                </ul>
            </div>
        </div>
    </div>

    
