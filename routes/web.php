<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', "TodosController@liste")->name("todo.list");

Route::post('/action/add', "TodosController@saveTodo")->name("todo.save");

Route::get('/action/done/{id}', "TodosController@markAsDone")->name("todo.done");

Route::get('/action/delete/{id}', "TodosController@deleteTodo")->name("todo.delete");

Route::get('/about', "AboutController@about")->name("todo.about");

Route::get('/vue', "TodosController@homevue");
