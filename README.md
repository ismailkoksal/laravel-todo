# Application Todo List
Il s'agit d'un projet scolaire

Cet application web a pour but de faire de la prise de note de « TODO » ou aussi appelé liste de tâches.

Une TODO List est un procédé qui se veut simple et efficace pour gérer les tâches d'un projet. Ces tâches peuvent être indépendantes ou devoir, au contraire, être accomplies dans un certain ordre.

Voilà la liste des fonctionnalités de l’application :

* Lister les tâches.
* Ajouter une tâche.
* Marquer comme terminé une tâche.
* Suppression d'une tâche.

## Introduction

Ces instructions vous permettront d'obtenir une copie du projet en cours d'exécution sur votre machine locale à des fins de développement et de test.

### Prérequis

Ce dont vous avez besoin pour installer le logiciel et comment les installer

```
PHP7
```
[Installation de Wamp.](http://www.wampserver.com/fr/#download-wrapper)
Installer Wamp, et vérifier que celui-ci fonctionne correctement.  
Activer l’extension php_openssl.dll dans la liste des extensions PHP.

```
Composer
```
[Télécharger Composer pour Windows](https://getcomposer.org/Composer-Setup.exe), lors de l’installation il vous sera demandé de séléctionner l’éxecutable PHP. ATTENTION: Bien séléctionner la version 7.1 minimum de PHP dans le dossier C:\wamp\bin\php\php\7.1.X\bin\php.exe <= Attention à prendre la bonne version  
Vérifier que la commande est bien disponible en tappant composer dans un terminal

```
Laravel
```
Installer laravel en tappant la commande ```$ composer global require "laravel/installer"```

### Configuration

Créer un fichier .env à partir du fichier .env.exemple dans la racine du projet.  
Modifier les lignes correspondant comme ceux-là :

```
APP_NAME=Todolist
DB_CONNECTION=sqlite
DB_DATABASE=../database/database.sqlite
```

### Tester

Laravel intègre un serveur de développement ce qui permet de le tester rapidement. Le lancement se fait via la commande suivante :
```
$ php artisan serve
```

## Outils

* [PHP7](http://www.php.net/) - Langage de programmation
* [Composer](https://getcomposer.org/) - Logiciel de gestionnaire de dépendances libre pour PHP
* [Laravel](http://www.dropwizard.io/1.0.2/docs/) - Framework PHP

## Versioning

J'utilise [Git](https://git-scm.com/) et [GitKraken](https://www.gitkraken.com/) pour le versioning.

## Auteurs

* [Ismail Koksal](https://gitlab.com/ismailkoksal)

## License

Ce projet est sous licence MIT - voir le fichier [LICENSE.md](LICENSE.md) pour plus de détails
